package pl.dfl.javafxtwowindowsexample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import pl.dfl.javafxtwowindowsexample.AppStart;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SecondWindowController implements Initializable {

    @FXML
    private Button openFirstWindowBtn;

    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void initialize(URL location, ResourceBundle resources) {
        openFirstWindowBtn.setOnAction(event -> openFirstWindow());
    }

    public void openFirstWindow(){
        try {
            Stage stage = new Stage();
            stage.setTitle("First window");

            FXMLLoader fxmlLoader = new FXMLLoader(AppStart.class.getResource("/view/FirstWindowView.fxml"));
            Parent root = fxmlLoader.load();
            FirstWindowController firstWindowController = fxmlLoader.getController();
            firstWindowController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

            this.stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
