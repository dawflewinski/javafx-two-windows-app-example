package pl.dfl.javafxtwowindowsexample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.dfl.javafxtwowindowsexample.controller.FirstWindowController;

public class AppStart extends Application {

    public static void main(String[] args) {
        Application.launch(AppStart.class);
    }

    public void start(Stage primaryStage) throws Exception {
        //opening the first window
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FirstWindowView.fxml"));
        Parent root = fxmlLoader.load();
        FirstWindowController firstWindowController = fxmlLoader.getController();
        firstWindowController.setStage(primaryStage);

        Scene firstScene = new Scene(root);

        primaryStage.setScene(firstScene);
        primaryStage.show();
    }
}
